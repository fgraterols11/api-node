module.exports = (app) => {
    const news = require('../controllers/new.controller.js');

    // Create a new new
    app.post('/news', news.create);

    // Retrieve all news
    app.get('/news', news.findAll);

    // Retrieve a single new with newId
    app.get('/news/:newId', news.findOne);

    // Update a new with newId
    app.put('/news/:newId', news.update);

    // Delete a new with newId
    app.delete('/news/:newId', news.delete);
}