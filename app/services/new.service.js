var Request = require("request");
const cron = require("node-cron");
const fs = require("fs");
const New = require('../models/new.model.js');



cron.schedule("0 * * * *", function() {
   getNews();
});

exports.getNews = function() {
    Request.get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs", (error, response, body) => {
        if(error) {
            return console.dir(error);
        }
        hackerNews = JSON.parse(body)
        console.log(hackerNews.hits.length)
        hackerNews.hits.forEach(element => {
            if (element.title || element.story_title) {
                New.find({ 'created_at': element.created_at }, function (err, docs) {
                    console.log(docs.length);
                    if (docs.length > 0) {
                        console.log('doesnt create');
                    } else {
                        console.log('create');
                        createNew(element);
                        
                    }
                });
            }
        });
    });
}


createNew = function(hackerNew) {
    const news = new New({
        title: hackerNew.title || hackerNew.story_title, 
        autor: hackerNew.author,
        url: hackerNew.story_url || hackerNew.url,
        created_at: hackerNew.created_at
    });

    // Save Note in the database
    news.save()
    .then(data => {
        return data;
    }).catch(err => {
        return false;
    });
}