const mongoose = require('mongoose');

const NewSchema = mongoose.Schema({
    title: String,
    autor: String,
    url: String,
    created_at: String
}, {
    timestamps: true
});

module.exports = mongoose.model('New', NewSchema);