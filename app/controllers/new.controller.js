const New = require('../models/new.model.js');

// Create and Save a new Note
exports.create = (req, res) => {
    if((!req.body.story_title && !req.body.title)) {
        return res.status(400).send({
            message: "New content can not be empty"
        });
    }

    const _news = new New({
        title: req.body.title || req.body.story_title, 
        autor: req.body.author,
        url: req.body.url,
        created_at: req.body.created_at
    });

    // Save Note in the database
    _news.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the NEw."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    New.find()
    .then(_news => {
        res.send(_news);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving news."
        });
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    New.findById(req.params.newId)
    .then(_news => {
        if(!_news) {
            return res.status(404).send({
                message: "New not found with id " + req.params.newId
            });            
        }
        res.send(_news);
    }).catch(err => {
        console.log(err)
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "New not found with id " + req.params.newId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving new with id " + req.params.newId
        });
    });
};


exports.update = (req, res) => {

};


exports.delete = (req, res) => {
    New.findByIdAndDelete(req.params.newId)
    .then(_news => {
        if(!_news) {
            return res.status(404).send({
                code: '200',
                message: "New not found with id " + req.params.newId
            });
        }
        res.send({message: "New deleted successfully!"});
    }).catch(err => {
        console.log(err);
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                code: '404',
                message: "New not found with id " + req.params.newId
            });                
        }
        return res.status(500).send({
            code: '404',
            message: "Could not delete new with id " + req.params.newId
        });
    });
};