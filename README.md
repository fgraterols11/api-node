# api-node

**Prerequisite:**

*  Node.JS
*  MongoDB

**STEPS TO RUN API**

1. Clone the Repository
2. Please install MongoDB in your machine if you have not done already. Checkout the official [MogngoDB installation manual](https://docs.mongodb.com/manual/administration/install-community/) for any help with the installation.
3. Run command `npm install`
4. Run command `node server.js`

